import boto3
from botocore.client import Config
import json
import warnings
import argparse
import os
print("starting execution")
# warnings.filterwarnings("ignore")

parser = argparse.ArgumentParser(description="Python based command line utility to 1. scale out and scale in EKS cluster node groups. 2. Stop and Start RDS Instances.")
parser.add_argument("--scale-out",default=False,action='store_true',help='pass this flag to scale out the node group')
parser.add_argument("--scale-in",default=False,action='store_true',help='pass this flag to scale in the node group')
parser.add_argument("--stop-rds",default=False,action='store_true',help="pass this flag to stop RDS clusters/instances")
parser.add_argument("--start-rds",default=False,action='store_true',help="pass this flag to start RDS clusters/instances")
parser.add_argument("--config-file",default="config.json",help="path to config file")
parser.add_argument("--dry-run",default=False,action='store_true',help='pass this flag to perform dry run')
args = parser.parse_args()
config = Config(connect_timeout=5, retries={'max_attempts': 2})

session = boto3.Session()
eks_client = session.client('eks', config=config, use_ssl=True, verify=False)
rds_client = session.client('rds', config=config, use_ssl=True, verify=False)
autoscaling_client = session.client('autoscaling', config=config, use_ssl=True, verify=False)

try:
    config_file = open(args.config_file,"r")
    eks_clusters = json.loads(config_file.read())["eks_clusters"]
    rds_clusters = json.loads(config_file.read())["rds"]["clusters"]
    rds_instances = json.loads(config_file.read())["rds"]["instances"]
    config_file.close()
except:
    print("Config file not found. Exiting.")
    os._exit(0)

def get_capacity(scale_out_flag, scale_in_flag, cluster_name):
    if scale_out_flag:
        min_count=eks_clusters[cluster_name]["scale_out_capacity"]["min_count"]
        desired_count=eks_clusters[cluster_name]["scale_out_capacity"]["desired_count"]
        max_count=eks_clusters[cluster_name]["scale_out_capacity"]["max_count"]
        print("Scaling out "+cluster_name)
        print("Cluster "+cluster_name+", will be scaled to : Min Count - "+str(min_count)+", Desired Count - "+str(desired_count)+", Max Count - "+str(max_count))
    elif scale_in_flag:
        min_count=eks_clusters[cluster_name]["scale_in_capacity"]["min_count"]
        desired_count=eks_clusters[cluster_name]["scale_in_capacity"]["desired_count"]
        max_count=eks_clusters[cluster_name]["scale_in_capacity"]["max_count"]
        print("Scaling in "+cluster_name)
        print("Cluster "+cluster_name+", will be scaled to : Min Count - "+str(min_count)+", Desired Count - "+str(desired_count)+", Max Count - "+str(max_count))
    capacity = {
        "min_count" : min_count,
        "desired_count" : desired_count,
        "max_count" : max_count
    }
    return capacity

if args.scale_in or args.scale_out:
    for cluster in eks_clusters:
        capacity = get_capacity(args.scale_out,args.scale_in,cluster)
        nodegroups = eks_client.list_nodegroups(
            clusterName=cluster,
        )["nodegroups"]
        for nodegroup in nodegroups:
            auto_scaling_groups = eks_client.describe_nodegroup(
                clusterName=cluster,
                nodegroupName=nodegroup
            )["nodegroup"]["resources"]["autoScalingGroups"]
            for auto_scaling_group in auto_scaling_groups:
                if not args.dry_run:
                    print("Updating: "+auto_scaling_group["name"])
                    response = autoscaling_client.update_auto_scaling_group(
                        AutoScalingGroupName=auto_scaling_group["name"],
                        MinSize = capacity["min_count"],
                        DesiredCapacity = capacity["desired_count"],
                        MaxSize = capacity["max_count"],
                    )
                elif args.dry_run:
                    print("Dry run | Updating: "+auto_scaling_group["name"])
    
for cluster in rds_clusters:
    if args.stop_rds:
        if not args.dry_run:
            print("Stopping RDS Cluster : "+cluster)
            rds_client.stop_db_cluster(
                DBClusterIdentifier=cluster
            )
        elif args.dry_run:
            print("Dry Run | Stopping RDS Cluster : "+cluster)
    elif args.start_rds:
        if not args.dry_run:
            print("Starting RDS Cluster : "+cluster)
            rds_client.start_db_cluster(
                DBClusterIdentifier=cluster
            )
        elif args.dry_run:
            print("Dry Run | Starting RDS Cluster : "+cluster)

for instance in rds_instances:
    if args.stop_rds:
        if not args.dry_run:
            print("Stopping RDS Instance : "+instance)
            rds_client.stop_db_instance(
                DBInstanceIdentifier=instance
            )
        elif args.dry_run:
            print("Dry Run | Stopping RDS Instance : "+instance)
    elif args.start_rds:
        if not args.dry_run:
            print("Starting RDS Instance : "+instance)
            rds_client.start_db_instance(
                DBInstanceIdentifier=instance
            )
        elif args.dry_run:
            print("Dry Run | Starting RDS Instance : "+instance)